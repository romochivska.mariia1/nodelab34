const gradeModel = require('../models/grade.model');

async function create(grade) {
    return gradeModel.create(grade);
}

async function fetchGrades({ searchString = '', page = 1, perPage = 20 }) {
    const filter = {
        lastName: { $regex: `^${searchString}`, $options: ''  }
    };

    return {
        items: await (gradeModel.find(filter)).skip((page - 1) * perPage).limit(Number(perPage)),
        count: await gradeModel.countDocuments(filter),
    }
}


async function getGrade(id) {
    return gradeModel.findById(id);
}

async function updateGrade(id, update) {
    return gradeModel.findByIdAndUpdate(id, update, { upsert: false, new: true });
};

async function deleteGrade(id) {
    return gradeModel.findByIdAndDelete(id);
};
async function findOne(filter) {
    return gradeModel.findOne(filter);
}

module.exports = {
    create,
    fetchGrades,
    getGrade,
    updateGrade,
    deleteGrade,
    findOne,
};
