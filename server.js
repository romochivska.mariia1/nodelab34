const express = require('express');
const mongoose = require('mongoose');
const { port, mongodb_uri } = require('./config');
const gradesRouter = require('./routes/grades.route'); 


mongoose.connect(mongodb_uri)
  .then(() => {
    console.log('Mongo DB connected');
  });

const app = express();

app.use(express.json());

app.use('/grades', gradesRouter); 

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
