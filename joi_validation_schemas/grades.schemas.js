const Joi = require('joi');


const GradeCreateSchema = Joi.object({

    lastName: Joi.string()
        .min(2)
        .max(60),
    
    group: Joi.string()
        .min(1)
        .max(60),

    subject: Joi.string()
        .required(),

    ticketNumber: Joi.number()
        .required(),

    grade: Joi.number()
        .required(),

    teacher: Joi.string()
        .required(),
});

const GradeUpdateSchema = Joi.object({

    lastName: Joi.string()
        .min(2)
        .max(60),
    
    group: Joi.string()
        .min(1)
        .max(60),

    subject: Joi.string()
        .required(),

    ticketNumber: Joi.number()
        .required(),

    grade: Joi.number()
        .required(),

    teacher: Joi.string()
        .required(),
});

module.exports = {
    GradeCreateSchema,
    GradeUpdateSchema,
};