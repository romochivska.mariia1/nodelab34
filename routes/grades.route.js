const express = require('express');
const router = express.Router();

const controller = require('../controllers/grades.controller'); 
const middleware = require('../middlewares/grades.middleware');

router.route('/')
    .get(controller.getGrades)
    .post(middleware.gradeCreationDataValidation,controller.createGrade);

router.route('/:gradeId')
    .get(middleware.gradeByIdValidation, controller.getGrade)
    .patch(middleware.gradeByIdValidation, controller.updateGrade)
    .delete(middleware.gradeByIdValidation, controller.deleteGrade);

module.exports = router;
