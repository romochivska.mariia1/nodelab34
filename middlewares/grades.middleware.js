const createError = require('http-errors');
const ObjectId = require('mongoose').Types.ObjectId;
const gradeService = require('../services/grades.service');
const { GradeCreateSchema, GradeUpdateSchema } = require('../joi_validation_schemas/grades.schemas');

async function gradeByIdValidation(req, res, next) {
    try {
        const { gradeId } = req.params;

        if (!ObjectId.isValid(gradeId)) {
            throw createError.BadRequest("Grade id is not valid");
        }

        const grade = await gradeService.getGrade(gradeId);

        if (!grade) {
            throw createError.NotFound("Grade with such id not found");
        }

        next();
    } catch(err) {
        next(err);
    }
};
const gradeCreationDataValidation = async (req, res, next) => {
    try {
        const { error } = GradeCreateSchema.validate(req.body);

        if (error) {
            throw createError.BadRequest(error.details[0].message);
        }

        const grade = await gradeService.findOne({
            $or: [
                { ticketNumber: req.body.ticketNumber },
                { subject: req.body.subject },
            ]
        });

        if (grade) {
            throw createError.BadRequest("User with such ticketNumber or subject already exist");
        }

        next();
    } catch (err) {
        next(err);
    }
};





module.exports = {
    gradeByIdValidation,
    gradeCreationDataValidation
  
    
};